#!/bin/bash

path="$(cat /tmp/mindshot.path)/figures"
filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"
if [ $extension != "png" ]
then
    convert $1 $path/$filename.png
    echo "Extension was $extension, had to convert"
else
    cp $1 $path
fi
echo "\\cImg{0.85}{${filename%.png}}{#@caption@#}{#@label@#}" | xclip -selection c

