#!/bin/bash 

path="$(cat /tmp/mindshot.path)"
send_content_path="$(cat ~/.config/mindshot/send_content_path)"
email="$(cat ~/.config/mindshot/email)"
token="$(cat ~/.config/mindshot/token)"
tex="$(cat $path/tex.tex)"

echo "Send-content-path: " $send_content_path
echo $email
echo $token
echo $path
echo $tex

curl -H "X-User-Email: $email" \
    -H "X-User-Token: $token" \
    -F "micropost[tex]=$tex" \
    -F "micropost[content_archive]=@$path.tar.gz" \
    $send_content_path
