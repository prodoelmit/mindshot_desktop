#!/usr/bin/env ruby

require 'fileutils'

timestamp = Time.now.strftime('%Y-%m-%d_%H-%M-%S_%L')
path = "/tmp/mindshot_#{timestamp}"
FileUtils.mkdir_p("#{path}/figures")
FileUtils.touch("#{path}/tex.tex")

File.open('/tmp/mindshot.path', 'w') do |f|
    f << path
end
