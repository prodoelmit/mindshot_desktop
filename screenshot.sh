#!/bin/bash

path="$(cat /tmp/mindshot.path)/figures"
filename=$(scrot -s '/tmp/%Y-%m-%d::%T.png' -e "mv \$f $path; echo \$n")
echo "\\cImg{0.85}{${filename%.png}}{#@caption@#}{#@label@#}" | xclip -selection c
