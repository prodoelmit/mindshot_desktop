#!/bin/bash

path="$(cat /tmp/mindshot.path)/figures"
imagedir="$1"
filename="$(ls $imagedir -1t | head -n1)"
image="$1/$filename"
extension="${filename##*.}"
filename="${filename%.*}"
if [ $extension != "png" ]
then
    convert $image $path/$filename.png
    echo "Extension was $extension, had to convert"
else
    cp $image $path
fi
echo "\\cImg{0.85}{${filename%.png}}{#@caption@#}{#@label@#}" | xclip -selection c

echo "$filename.$extension"

